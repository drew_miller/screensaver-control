= PausumeService =

== About ==
PausumeService is a service that listens to the internal audio playing on your computer, and tells the computer not to go to sleep if the sound power passes beyond a specified threshold. PausumeService can be interacted with via its DBus session interface at org.pausume, where it broadcasts the power of the PulseAudio sound playing. PausumeService is intended to be configured with PausumeClient.

== System Requirements ==
You need to have the following libraries installed:
 * libpulse.
 * libqt5dbus5. You could probably also use libqt4-dbus, but you would have to replace a few lines in the CMakeLists.txt  file to work with QT4).
 * libnotify. You need at least the version installed with Ubuntu 13.04. Earlier libnotify versions don't support clicking on notifications to dismiss them, or having buttons in notifications.

== Building the program ==

=== Ninja ===
Building with Ninja requires that you have ninja-build installed (NOT "ninja", which is a different program entirely). You can install the Ninja build service from the repositories:
sudo apt-get install ninja-build

After installing ninja-build, cmake may still look in the incorrect directory (/usr/sbin) for ninja. This seems like a bug to me, but you can work around it with:
sudo ln -s /usr/bin/ninja /usr/sbin/ninja

Once you have Ninja build installed, from the directory containing this README.txt file:
cd build
cmake -G Ninja .. && ninja

=== Make ===
I recommend using Ninja because it's faster, but you can also build the program with Make:
cd build
cmake .. & ninja

== Running the program ==
build/PausumeService

== Checking that it's working ==
Inspect the output of the program with qdbusviewer or d-feet. qdbusviewer has an issue where it can't be properly added to the Unity system menu, but you can start it up with Alt+F2. The advantage qdbusviewer has over d-feet is that qdbusviewer can listen to signal outputs.

sudo apt-get install qdbusviewer
qdbusviewer

Then search for org.PausumeService on the D-Bus with qdbusviewer, and run its methods. You should be able to set the observationPeriod, which is how frequently (seconds per DBus broadcast) that the program broadcasts the power of the Pulseaudio system to the D-Bus. The recommended value for this is 5.

// Drew Miller.
dmiller309@gmail.com

Resources:
 * DBus clients:
 	http://techbase.kde.org/Development/Tutorials/D-Bus/Accessing_Interfaces
 * DBus interfaces:
 	http://techbase.kde.org/Development/Tutorials/D-Bus/Creating_Interfaces
 * DBus watcher:
 	Better than continually listing the services on the DBus to see if a new audio player has connected.
 	http://qt-project.org/doc/qt-4.8/qdbusservicewatcher.html
 * QThread:
 	used because the DBus locks up if it's running in the same thread as the PulseService
 	http://stackoverflow.com/questions/11033971/qt-thread-with-movetothread
 	http://qt-project.org/doc/qt-4.8/qthread.html
 * Signals and Slots:
 	The basic mechanism used by Qt to communicate between threads.
 	http://qt-project.org/doc/qt-4.8/signalsandslots.html
 * Ninja build system:
 	http://neugierig.org/software/chromium/notes/2011/02/ninja.html
 * Taking a screenshot with XLib:
	http://stackoverflow.com/questions/8249669/how-do-take-a-screenshot-correctly-with-xlib
 * Recording audio from PulseAudio:
	http://stackoverflow.com/questions/14454852/pulseaudio-recording-and-playback-fails
