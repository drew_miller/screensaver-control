#ifndef DBOUT
#ifdef DEBUG_BUILD
#define DBOUT(x) \
   qDebug() << x
#else
#define DBOUT(x)
#endif
#endif
