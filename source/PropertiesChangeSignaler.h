#pragma once

#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QVariant>

class PropertiesChangeSignaler: public QObject {
Q_OBJECT

public:
	explicit PropertiesChangeSignaler(QString source, QObject *parent = 0);
	QString getSource();

public slots:
	void propertiesChanged_slot(QString interface_name,
			QMap<QString, QVariant> changed_properties);

signals:
	void propertiesChanged(QString source, QString interface_name,
			QMap<QString, QVariant> changed_properties);

private:
	QString source;

};
