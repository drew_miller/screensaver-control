#pragma once

#ifdef QT_NO_DEBUG
#   define X_ASSERT(x) (x)
#else
#   define X_ASSERT(x) Q_ASSERT(x)
#endif

