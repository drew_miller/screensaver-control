#include "PulseDevicePower.h"
#include "dbout.h"

#include <cmath>
#include <iostream>
#include <limits>
#include <signal.h>
#include <stdio.h>
#include <pulse/error.h>
#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include <QtCore/QDebug>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#include <QtCore/QString>

struct PA_STATE {
    double duration;
    pa_mainloop_api *mainloopApi;
    QMap<QString, double> *monitorPowers;
    QMutex *mutex;
    QWaitCondition *waitCondition;
    int sinksWaiting;
};

// A shortcut for terminating the application
void quit(pa_mainloop_api* mainloop_api, int ret) {
	assert(mainloop_api);
	mainloop_api->quit(mainloop_api, ret);
}

void context_drain_complete(pa_context*c, void *userdata) {
	pa_context_disconnect(c);
}

double pa_get_source_power(QString device, double duration) {
	int error = 0;
	pa_sample_spec sampleSpec;
	sampleSpec.channels = 1;
	sampleSpec.format = PA_SAMPLE_S32NE;
	sampleSpec.rate = 44100;
	pa_simple* s_in = pa_simple_new(NULL, "ScreensaverControl", PA_STREAM_RECORD,
			device.toUtf8().constData(), "pa_get_source_power", &sampleSpec,
			NULL, NULL, &error);
    double power = 0;
    if (!error) {
        int bufferLength = (int) (sampleSpec.rate * sampleSpec.channels * duration);
        int buffer[bufferLength];
        //	DBOUT("pa_simple_read");
        pa_simple_read(s_in, buffer, sizeof(buffer), &error);
        pa_simple_free(s_in);
        double mean = 0;
        for (int i = 0; i < bufferLength; i++) {
            mean += buffer[i];
        }
        mean /= bufferLength;
        for (int i = 0; i < bufferLength; i++) {
            double normalizedSample = buffer[i] - mean;
            if (normalizedSample < 0) {
                normalizedSample *= -1;
            }
            power += normalizedSample;
        }
        power /= bufferLength;
        power /= INT_MAX;
        DBOUT("Device:" << device << " Power:" << power);
    }
    return power;
}

void pa_sinklist_cb(pa_context *c, const pa_sink_info *l, int eol, void *userdata) {
	// Cast userdata back to the parameter list.
    PA_STATE* paramMapPtr = (PA_STATE*) userdata;
    QMutex* mutex = paramMapPtr->mutex;
    QWaitCondition* waitCondition = paramMapPtr->waitCondition;
    QMap<QString, double> *monitorPowers = paramMapPtr->monitorPowers;
	if (eol == 0) {
		mutex->lock();
        paramMapPtr->sinksWaiting++;
		mutex->unlock();
        double power = pa_get_source_power(l->monitor_source_name, paramMapPtr->duration);
        mutex->lock();
        (*monitorPowers)[l->name] = power;
        paramMapPtr->sinksWaiting--;
        if (paramMapPtr->sinksWaiting <= 0) {
            waitCondition->wakeAll();
		}
		mutex->unlock();
	} else {
		mutex->lock();
        while (paramMapPtr->sinksWaiting > 0) {
            waitCondition->wait(mutex);
		}
		mutex->unlock();
		pa_operation* o = pa_context_drain(c, context_drain_complete, NULL);
		if (!o) {
			pa_context_disconnect(c);
		} else {
			pa_operation_unref(o);
		}
	}
}

void pa_state_cb(pa_context *c, void* userdata) {
    // Read the userdata parameters.
    PA_STATE* paramMapPtr = (PA_STATE*) userdata;
    pa_context_state_t state = pa_context_get_state(c);
	if (state == PA_CONTEXT_READY) {
		// Define the pa_sinklist_cb parameters.
		// Get the sink info.
		pa_operation* o;
        if (!(o = pa_context_get_sink_info_list(c, pa_sinklist_cb, paramMapPtr))) {
//        	delete datalist;
			std::cerr << "pa_context_get_sink_info_list() failed." << std::endl;
			std::cerr << pa_strerror(pa_context_errno(c)) << std::endl;
		} else {
			pa_operation_unref(o);
		}
		DBOUT("Got the sinklist");
    } else if (state == PA_CONTEXT_TERMINATED || state == PA_CONTEXT_FAILED) {
		// Context cleanly closed
		// Unreference the values added to datalist
		// Quit the context.
        pa_mainloop_api* pa_ml = (pa_mainloop_api*) paramMapPtr->mainloopApi;
        int ret = (state != PA_CONTEXT_TERMINATED);
        quit(pa_ml, ret);
	}
}

int pa_get_monitor_power_map(QMap<QString, double> *monitorPowers, double duration) {
	// Create a mainloop API and connection to the default server
	pa_mainloop *pa_ml = pa_mainloop_new();
	pa_mainloop_api *pa_mlapi = pa_mainloop_get_api(pa_ml);
	// Handle the signal to kill the process
	int r = pa_signal_init(pa_mlapi);
	assert(r == 0);
	// TODO: Handle Ctrl+C to cleanly disconnect from the pulseaudio server.
//    pa_signal_new(SIGINT, exit_signal_callback, NULL);
//#ifdef SIGPIPE
//    signal(SIGPIPE, SIG_IGN);
//#endif
	pa_context *pa_ctx = pa_context_new(pa_mlapi, "PulseDeviceList");
    QMutex mutex;
    QWaitCondition waitCondition;
    // Make the parameter list to pass to the pa_state_cb
    PA_STATE paramMap;
    paramMap.monitorPowers = monitorPowers;
    paramMap.duration = duration;
    paramMap.mainloopApi = pa_mlapi;
    paramMap.mutex = &mutex;
    paramMap.waitCondition = &waitCondition;
    paramMap.sinksWaiting = 0;
    pa_context_set_state_callback(pa_ctx, pa_state_cb, &paramMap);
    if (pa_context_connect(pa_ctx, NULL, PA_CONTEXT_NOFLAGS, NULL) >= 0) {
        // Run the mainloop.
        int error = 0;
        pa_mainloop_run(pa_ml, &error);
        if (error) {
            DBOUT("pa_mainloop_run error: " << error);
        }
    }
    if (pa_ctx) {
        pa_context_unref(pa_ctx);
    }
    if (pa_ml) {
        pa_signal_done();
        pa_mainloop_free(pa_ml);
    }
	return 0;
}
