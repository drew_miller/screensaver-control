#include "ScreensaverControlService.h"
#include "ScreensaverControlAdaptor.h"
#include "PulseService.h"
#include "PropertiesChangeSignaler.h"
#include "dbout.h"
#include "assert.h"
#include "Config.h"

#include <iostream>
#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QMap>
#include <QtCore/QMapIterator>
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QSettings>
#include <QtCore/QThread>
#include <QtCore/QVariant>
#include <QtDBus/QDBusArgument>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusConnectionInterface>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QDBusReply>
#include <QtDBus/QDBusServiceWatcher>
#include <QtDBus/QtDBus>
#include <unistd.h>

ScreensaverControlService::ScreensaverControlService(QSettings* settings,
		QObject *parent) :
		QObject(parent) {
	this->settings = settings;
	lastUnpause = new QDateTime();
	numPlayingPlayers = 0;
	isScreensaverInhibited = false;
	isScreensaverActive = false;
	screensaverInhibitCookie = false;
	isNotificationShowing = false;
	mutedByScreensaver = false;
	notificationId = 0;
	pulseService = NULL;
	pulseService_thread = NULL;
}

ScreensaverControlService::~ScreensaverControlService() {
	if (pulseService != NULL) {
		pulseService->stop();
	}
	if (pulseService_thread != NULL) {
		pulseService_thread->wait();
	}
	delete pulseService;
	pulseService = NULL;
	delete pulseService_thread;
	pulseService_thread = NULL;
	if (isScreensaverInhibited) {
		setScreensaverInhibited(false);
	}
	for (PropertiesChangeSignaler* signaler : signalers.values()) {
		delete signaler;
	}
	delete lastUnpause;
	DBOUT("ScreensaverControlService destroyed.");
}

void ScreensaverControlService::run() {
	// Register the DBus service.
	new ScreensaverControlAdaptor(this);
	if (dbus.registerService(SCREENSAVER_SERVICE)) {
		DBOUT("dBus service registered:" << SCREENSAVER_SERVICE);
		if (dbus.registerObject(SCREENSAVER_OBJECT, this)) {
			DBOUT("dBus object registered:" << SCREENSAVER_OBJECT);
		} else {
            std::cerr << "Failed to register object" << SCREENSAVER_OBJECT << std::endl;
			exit(1);
		}
	} else {
		std::cerr << "Failed to register service" << SCREENSAVER_SERVICE
				<< std::endl;
		exit(1);
	}

	// Communication between this and pulseService
	pulseService_thread = new QThread();
	pulseService = new PulseService(getAudioObservationPeriod());
	pulseService->moveToThread(pulseService_thread);
    X_ASSERT(connect(pulseService, SIGNAL(powerMeasured(double)),
                     this, SLOT(handle_powerMeasured(double))));
    pulseService->setObservationPeriod(getAudioObservationPeriod());
	if (getAudioInhibitsScreensaver()) {
        X_ASSERT(connect(pulseService_thread, SIGNAL(started()),
                         pulseService, SLOT(run())));
    }

	pulseService_thread->start();

	// Listen for changes to this application's notification
    if(!dbus.connect(
                "org.freedesktop.Notifications",
                "/org/freedesktop/Notifications", "org.freedesktop.Notifications",
                "ActionInvoked",
                this,
                SLOT(handle_notificationActionInvoked(uint, QString)))) {
        std::cerr
                << "Failed to connect to DBus org.freedesktop.notifications ActionInvoked signal."
                << std::endl;
    }
    if(!dbus.connect(
                "org.freedesktop.Notifications",
                "/org/freedesktop/Notifications",
                "org.freedesktop.Notifications",
                "NotificationClosed", this,
                SLOT(handle_notificationClosed(uint, uint)))) {
        std::cerr
                << "Failed to connect to DBus org.freedesktop.notifications ActionClosed signal."
                << std::endl;
    }

	// Listen for when services enter or leave the DBus.
	// TODO: If possible, use a DBus filter.
    X_ASSERT(dbus.connect(
                 "org.freedesktop.DBus",
                 "/org/freedesktop/DBus",
                 "org.freedesktop.DBus",
                 "NameOwnerChanged",
                 this,
                 SLOT(handle_dBusNameOwnerChanged(QString, QString, QString))));
    // Add the current list of media players to the DBus
	const QDBusConnectionInterface* dbus_interface = dbus.interface();
	const QDBusReply<QStringList> serviceNames_reply =
			dbus_interface->registeredServiceNames();
	for (QString serviceName : serviceNames_reply.value()) {
		if (serviceName.startsWith("org.mpris.MediaPlayer2.")) {
			dBusServiceRegistered(serviceName);
		}
	}

	// Listen for the screensaver state change signal
    if(
        dbus.connect(
            "com.canonical.Unity",
            "/com/canonical/Unity/Session",
            "com.canonical.Unity.Session",
            "Locked",
            this,
            SLOT(handle_screenLocked())) &&
        dbus.connect(
            "com.canonical.Unity",
            "/com/canonical/Unity/Session",
            "com.canonical.Unity.Session",
            "Unlocked",
            this,
            SLOT(handle_screenUnlocked()))) {
        DBOUT("Connected to com.canonical.Unity/com/canonical/Unity/Session Locked signal.");
        DBOUT("Connected to com.canonical.Unity/com/canonical/Unity/Session Unlocked signal.");
    } else if(
      dbus.connect(
          "org.gnome.ScreenSaver",
          "/org/gnome/ScreenSaver",
          "org.gnome.ScreenSaver",
          "ActiveChanged",
          this,
          SLOT(handle_screenSaverActiveChanged(bool)))) {
        DBOUT("Connected to org.gnome.ScreenSaver ActiveChanged signal.");
    } else {
        std::cerr << "Failed to connect to D-Bus screensaver state change signal."  << std::endl;
    }
    // Deal with the initial state of the screensaver.
    handle_screenSaverActiveChanged(getScreensaverState());

}

void ScreensaverControlService::resetToDefault() {
	resetPropertiesVersion();
	resetPauseForScreensaver();
	resetResumeAfterScreensaver();
	resetMuteForScreensaver();
	resetUnmuteAfterScreensaver();
	resetPlaybackInhibitsScreensaver();
	resetAudioInhibitsScreensaver();
	resetNotificationsEnabled();
	resetAudioSleepInhibitPower();
	resetAudioObservationPeriod();
	resetNotificationDuration();
}

QString ScreensaverControlService::getPlaybackStatus(QString player) {
	QDBusMessage msg = QDBusMessage::createMethodCall(player,
			"/org/mpris/MediaPlayer2", "org.freedesktop.DBus.Properties",
			"Get");
	QList<QVariant> args;
	args.append("org.mpris.MediaPlayer2.Player");
	args.append("PlaybackStatus");
	msg.setArguments(args);
	QDBusReply<QVariant> reply = dbus.call(msg);
	QString playbackStatus = reply.value().toString();
	return playbackStatus;
}

void ScreensaverControlService::setPlaybackStatus(QString player,
		QString status) {
	QString method = "";
	if (status == "Playing") {
		method = "Play";
	} else if (status == "Paused") {
		method = "Pause";
	} else if (status == "Stopped") {
		method = "Stop";
	}
	if (SCREENSAVER_TOTEM_WORKAROUND) {
		if (player.startsWith("org.mpris.MediaPlayer2.totem")) {
			if (status == "Playing") {
				QString currentPlaybackStatus = getPlaybackStatus(player);
				if (currentPlaybackStatus == "Paused"
						|| currentPlaybackStatus == "Stopped") {
					method = "PlayPause";
				} else {
					return;
				}
			} else if (status == "Paused") {
				QString currentPlaybackStatus = getPlaybackStatus(player);
				if (currentPlaybackStatus == "Playing") {
					method = "PlayPause";
				} else {
					return;
				}
			}
		}
	}
	QDBusMessage msg = QDBusMessage::createMethodCall(player,
			"/org/mpris/MediaPlayer2", "org.mpris.MediaPlayer2.Player", method);
	QDBusReply<uint> reply = dbus.call(msg);
}

void ScreensaverControlService::setScreensaverInhibited(bool inhibited) {
	if (inhibited) {
		if (!isScreensaverInhibited) {
			QDBusMessage msg = QDBusMessage::createMethodCall(
					"org.freedesktop.screensaver",
					"/org/freedesktop/ScreenSaver",
					"org.freedesktop.screensaver", "Inhibit");
			QList<QVariant>* args = new QList<QVariant>();
			args->append("ScreensaverControl");
			args->append("media playing");
			msg.setArguments(*args);
			QDBusReply<uint> reply = dbus.call(msg);
			screensaverInhibitCookie = reply.value();
			DBOUT("ScreenSaver inhibited.");
			delete args;
		}
	} else {
		if (isScreensaverInhibited) {
			QDBusMessage msg = QDBusMessage::createMethodCall(
					"org.freedesktop.screensaver",
					"/org/freedesktop/ScreenSaver",
					"org.freedesktop.screensaver", "UnInhibit");
			QList<QVariant>* args = new QList<QVariant>();
			args->append(screensaverInhibitCookie);
			msg.setArguments(*args);
			dbus.call(msg);
			DBOUT("Screensaver uninhibited.");
			delete args;
		}
	}
	isScreensaverInhibited = inhibited;
}

void ScreensaverControlService::showNotification() {
	QDBusMessage msg = QDBusMessage::createMethodCall(
			"org.freedesktop.Notifications", "/org/freedesktop/Notifications",
			"org.freedesktop.Notifications", "Notify");
	QList<QVariant> args;
	args.append("ScreensaverControlService");
	args.append(notificationId);
// TODO: Build an app_icon from the app icons of the players being controlled.
	QString app_icon =
			"/usr/share/icons/hicolor/256x256/apps/screensaver-control.png";
	QStringList actions;
	int numPausedPlayers = pausedPlayers.size();
	QString body = QString::number(numPausedPlayers) + " media player"
			+ (numPausedPlayers == 1 ? " was" : "s were")
			+ " paused by the screensaver.";
	if (getResumeAfterScreensaver()) {
		body.append(" Stop resumed playback?");
		actions.append("cancel_playback");
		actions.append("■ Stop");
		args.append(app_icon);
		args.append("Media playback resumed");
		args.append(body);
		args.append(actions);
		QVariantMap hints;
		args.append(hints);
	} else {
		body.append(" Resume playback?");
		actions.append("resume_playback");
		actions.append("▶ resume");
		args.append(app_icon);
		args.append("Media paused by screensaver");
		args.append(body);
		args.append(actions);
		QVariantMap hints;
		args.append(hints);
	}
	int notification_duration = getNotificationDuration();
	args.append(notification_duration);
	msg.setArguments(args);
	QDBusReply<uint> reply = dbus.call(msg);
	notificationId = reply;
	isNotificationShowing = true;
	DBOUT("Showing notification with Id =" << notificationId);
}

void ScreensaverControlService::closeNotification() {
	DBOUT("Closing notification with Id:" << notificationId);
// Close the notification.
	QDBusMessage msg = QDBusMessage::createMethodCall(
                "org.freedesktop.Notifications",
                "/org/freedesktop/Notifications",
                "org.freedesktop.Notifications",
                "CloseNotification");
    QList<QVariant> args;
	args.append(notificationId);
	msg.setArguments(args);
	dbus.call(msg);
// Reset the notificationId.
	notificationId = 0;
// Update isNotificationShowing
	isNotificationShowing = false;
}

void ScreensaverControlService::dBusServiceRegistered(QString name) {
	DBOUT("dBusServiceRegistered(" << name << ")");
	if (!signalers.contains(name)) {
		// Connect to the service to receive property change notifications
		PropertiesChangeSignaler* signaler = new PropertiesChangeSignaler(name,
				this);
		dbus.connect(name, "/org/mpris/MediaPlayer2",
				"org.freedesktop.DBus.Properties", "PropertiesChanged",
				signaler,
				SLOT(propertiesChanged_slot(QString, QMap<QString, QVariant>)));
		connect(signaler,
		SIGNAL(propertiesChanged(QString, QString, QMap<QString, QVariant>)),
				this,
                SLOT(handle_playerPropertiesChanged(QString, QString, QMap<QString, QVariant>)));
		signalers.insert(name, signaler);
		DBOUT("name =" << name.toUtf8().constData());
		// Check the service's playback initial playback status
		QString playbackStatus = getPlaybackStatus(name);
		playbackStatuses.insert(name, playbackStatus);
		// If the player is playing, inhibit the screensaver.
		if (playbackStatus == "Playing") {
			numPlayingPlayers++;
			if (getPlaybackInhibitsScreensaver()) {
				setScreensaverInhibited(true);
			}
			emit numPlayingPlayersCountChanged(numPlayingPlayers);
		}
	}
}

void ScreensaverControlService::dBusServiceUnregistered(QString name) {
// Remove the player's playback status
	DBOUT("dBusServiceUnregistered(" << name << ")");
	if (playbackStatuses.contains(name)) {
		if (playbackStatuses.value(name) == "Playing") {
			numPlayingPlayers--;
			if (getPlaybackInhibitsScreensaver()) {
				setScreensaverInhibited(true);
			}
			emit numPlayingPlayersCountChanged(numPlayingPlayers);
		}
		playbackStatuses.remove(name);
		DBOUT("unregistered: " << name);
	}
    // Free and disconnect
    delete signalers.value(name);
    // Remove from the map
    signalers.remove(name);
}

void ScreensaverControlService::handle_dBusNameOwnerChanged(QString name,
		QString old_owner, QString new_owner) {
	if (name.startsWith("org.mpris.MediaPlayer2.")) {
		DBOUT("Name owner changed( name =" << name << ", old_owner =" << old_owner << ", new_owner =" << new_owner << ")");
		if (old_owner == "") {
			dBusServiceRegistered(name);
		}
		if (new_owner == "") {
			dBusServiceUnregistered(name);
		}
	}
}

bool ScreensaverControlService::getScreensaverState() {
// Get the initial screensaver state
    QDBusMessage msg = QDBusMessage::createMethodCall(
                "org.gnome.ScreenSaver",
                "/org/gnome/ScreenSaver",
                "org.gnome.ScreenSaver",
                "GetActive");
    QDBusReply<bool> reply = dbus.call(msg);
	isScreensaverActive = reply.value();
	return isScreensaverActive;
}

void ScreensaverControlService::handle_powerMeasured(double power) {
    DBOUT("handlePowerMeasured( power =" << power << ", sleepInhibitPower= " << getAudioSleepInhibitPower() << " )");
    emit powerMeasured(power);
    if (power > getAudioSleepInhibitPower()) {
        // Don't simulate user activity if the screen is already alseep.
        if (!isScreensaverActive) {
            QDBusMessage msg = QDBusMessage::createMethodCall(
                        "org.gnome.ScreenSaver",
                        "/org/gnome/ScreenSaver",
                        "org.gnome.ScreenSaver",
                        "SimulateUserActivity");
            dbus.call(msg);
            DBOUT("Audio power =" << power << "Simulating user activity to prevent screensaver.");
        }
    }
}

void ScreensaverControlService::handle_screenLocked() {
    handle_screenSaverActiveChanged(true);
}

void ScreensaverControlService::handle_screenUnlocked() {
    handle_screenSaverActiveChanged(false);
}

void ScreensaverControlService::handle_screenSaverActiveChanged(bool state) {
// Don't run the pulse loop while the screesaver is on.
	DBOUT("handleScreenSaverActiveChanged( state =" << state << " )");
	isScreensaverActive = state;
	DBOUT("Screensaver state =" << state);
	if (state) {
		// Clear the notification if it hasn't gone away already.
		if (isNotificationShowing) {
			closeNotification();
		}
		// Clear the list of paused players and repopulate it.
		pausedPlayers.clear();
		if (getPauseForScreensaver()) {
			QMapIterator<QString, QString> it(playbackStatuses);
			while (it.hasNext()) {
				it.next();
				DBOUT("it.key() =" << it.key());
				if (it.value() == "Playing") {
					setPlaybackStatus(it.key(), "Paused");
					pausedPlayers.insert(it.key());
				}
			}
			for (QString pausedPlayer : pausedPlayers) {
				playbackStatuses.insert(pausedPlayer, "Paused");
			}
		}
		// Mute volume
		if (getMuteForScreensaver()) {
			if (!pulseService->isMute()) {
				pulseService->setMute(true);
				mutedByScreensaver = true;
			}
		}
		// Pause PulseService since it isn't used during screensaver.
		pulseService->pauseAsync();
	} else {
		// If the state of a media player started playing during the
		// screensaver, inhibit the screensaver.
		if (numPlayingPlayers > 0) {
			setScreensaverInhibited(true);
		}
		// Unpause media players
		unpausingPlayers.clear();
		if (pausedPlayers.size() > 0) {
			if (getResumeAfterScreensaver()) {
				unpausingPlayers.unite(pausedPlayers);
				for (const QString &player : pausedPlayers) {
					//TODO Rhythmbox will show a notification when it is paused
					// here. Kill that notification if ScreensaverControlService is going
					// to show it's own (redundant) notification a few lines
					// below.
					setPlaybackStatus(player, "Playing");
				}
			}
			if (getNotificationsEnabled()) {
				//TODO Rhythmbox will show a notification when it is paused
				// before going to screensaver. If the user logs back in
				// immediately after logging out (usually within 10 seconds),
				// the notification from Rhythmbox will still be showing.
				showNotification();
			}
		}
		// Unmute volume
		if (mutedByScreensaver && getUnmuteAfterScreensaver()) {
			pulseService->setMute(false);
		}
		mutedByScreensaver = false;
		// Resume PulseService
		if (getAudioInhibitsScreensaver()) {
			DBOUT("Unpausing pulseService.");
			pulseService->unpauseAsync();
		}
	}
}

// TODO: Also check the QStringList invalidated_properties.
// Adding QList<QString> or QStringList didn't work.
// Where to add invalidated_properties parameter:
//  * ScreensaverControlService.cpp dBusServiceRegistered function (both connect statements).
//  * ScreensaverControlService.cpp handlePlayerPropertiesChanged function (here).
//  * ScreensaverControlService.hpp handlePlayerPropertiesChanged function declaration.
//  * PropertiesChangeSignaler.cpp propertiesChanged_slot function.
//  * PropertiesChangeSignaler.hpp propertiesChanged_slot function declaration.
// It's kind of a pain, but the PropertiesChangeSignaler is necessary for using
// the Qt SIGNAL/SLOT mechanism because you can't connect a lambda function as
// a slot. Qt requires that SIGNALs and SLOTs are declared at compile time for
// the MOC process.
// http://stackoverflow.com/questions/15624800/connect-qml-signal-to-c11-lambda-slot-qt-5
// It may be possible to do this without an adaptor using the C++11 syntax.
// https://qt-project.org/wiki/New_Signal_Slot_Syntax
void ScreensaverControlService::handle_playerPropertiesChanged(QString source,
		QString interface_name, QMap<QString, QVariant> changed_properties) {
	DBOUT("handlePlayerPropertiesChanged( source =" << source << ")");
	if (changed_properties.contains("PlaybackStatus")) {
		if (!isScreensaverActive && isNotificationShowing) {
			if (unpausingPlayers.contains(source)) {
				unpausingPlayers.remove(source);
			} else {
				pausedPlayers.remove(source);
				if (pausedPlayers.size() == 0) {
					// Dismiss the notification
					closeNotification();
				} else {
					showNotification();
				}
			}
		}
		QString prevPlaybackStatus = playbackStatuses.value(source);
		QString playbackStatus =
				changed_properties.value("PlaybackStatus").toString();
		if (playbackStatus == "Playing") {
			if (prevPlaybackStatus != "Playing") {
				playbackStatuses.insert(source, playbackStatus);
				numPlayingPlayers++;
				if (getPlaybackInhibitsScreensaver()) {
					if (!isScreensaverActive) {
						setScreensaverInhibited(true);
					}
				}
				emit numPlayingPlayersCountChanged(numPlayingPlayers);
			}
		} else {
			if (prevPlaybackStatus == "Playing") {
				playbackStatuses.insert(source, playbackStatus);
				numPlayingPlayers--;
				if (getPlaybackInhibitsScreensaver()) {
					setScreensaverInhibited(false);
				}
				emit numPlayingPlayersCountChanged(numPlayingPlayers);
			}
		}
	}
}

void ScreensaverControlService::handle_notificationActionInvoked(uint id,
		QString action_key) {
	if (id == notificationId) {
		DBOUT("Notification action invoked:" << action_key);
		if (action_key == "cancel_playback") {
			// Re-pause all players paused by screensaver
			unpausingPlayers.clear();
			for (const QString &player : pausedPlayers) {
				setPlaybackStatus(player, "Paused");
			}
		} else if (action_key == "resume_playback") {
			// Unpause all players paused by screensaver
			for (const QString &player : pausedPlayers) {
				setPlaybackStatus(player, "Playing");
			}
		}
	}
}

void ScreensaverControlService::handle_notificationClosed(uint id, uint reason) {
	if (notificationId == id) {
		DBOUT("Notification closed. Reason:" << reason);
		isNotificationShowing = false;
		notificationId = 0;
	}
}

int ScreensaverControlService::countPlayingMediaPlayers() {
	return numPlayingPlayers;
}

// Properties //////////////////////////////////////////////////////////////////
// The next 170 lines of code could probably be reduced to 30 if I used a
// separate config file to store the default values and just loaded those. This
// approach would be problematic for setting things that require parts of
// ScreensaverControlService to be notified immediately when values are changed.

// reset all
void ScreensaverControlService::resetToDefaults() {
	resetPropertiesVersion();
	resetPauseForScreensaver();
	resetResumeAfterScreensaver();
	resetMuteForScreensaver();
	resetUnmuteAfterScreensaver();
	resetPlaybackInhibitsScreensaver();
	resetAudioInhibitsScreensaver();
	resetAudioSleepInhibitPower();
	resetAudioObservationPeriod();
	resetNotificationsEnabled();
	resetNotificationDuration();
}

// properties_version
int ScreensaverControlService::getPropertiesVersion() {
	return settings->value("properties_version", 0).toInt();
}
void ScreensaverControlService::setPropertiesVersion(int properties_version) {
	if (getPropertiesVersion() != properties_version) {
		settings->setValue("properties_version", properties_version);
		emit propertiesChanged("properties_version");
	}
}
void ScreensaverControlService::resetPropertiesVersion() {
	setPropertiesVersion(SCREENSAVER_PROPERTIES_VERSION_CURR);
}

// pause_for_screensaver
bool ScreensaverControlService::getPauseForScreensaver() {
	return settings->value("pause_for_screensaver",
	SCREENSAVER_DEFAULT_PAUSE_FOR_SCREENSAVER).toBool();
}
void ScreensaverControlService::setPauseForScreensaver(
		const bool &pause_for_screensaver) {
	if (getPauseForScreensaver() != pause_for_screensaver) {
		settings->setValue("pause_for_screensaver", pause_for_screensaver);
		emit propertiesChanged("pause_for_screensaver");
	}
}
void ScreensaverControlService::resetPauseForScreensaver() {
	setPauseForScreensaver(SCREENSAVER_DEFAULT_PAUSE_FOR_SCREENSAVER);
}

// resume_after_screensaver
bool ScreensaverControlService::getResumeAfterScreensaver() {
	return settings->value("resume_after_screensaver",
	SCREENSAVER_DEFAULT_RESUME_AFTER_SCREENSAVER).toBool();
}
void ScreensaverControlService::setResumeAfterScreensaver(
		bool resume_after_screensaver) {
	if (getResumeAfterScreensaver() != resume_after_screensaver) {
		settings->setValue("resume_after_screensaver",
				resume_after_screensaver);
		emit propertiesChanged("resume_after_screensaver");
	}
}
void ScreensaverControlService::resetResumeAfterScreensaver() {
	setResumeAfterScreensaver(SCREENSAVER_DEFAULT_RESUME_AFTER_SCREENSAVER);
}

// mute_for_screensaver
bool ScreensaverControlService::getMuteForScreensaver() {
	return settings->value("mute_for_screensaver",
	SCREENSAVER_DEFAULT_MUTE_FOR_SCREENSAVER).toBool();
}
void ScreensaverControlService::setMuteForScreensaver(
		bool mute_for_screensaver) {
	if (getMuteForScreensaver() != mute_for_screensaver) {
		settings->setValue("mute_for_screensaver", mute_for_screensaver);
		emit propertiesChanged("mute_for_screensaver");
	}
}
void ScreensaverControlService::resetMuteForScreensaver() {
	setMuteForScreensaver(SCREENSAVER_DEFAULT_MUTE_FOR_SCREENSAVER);
}

// unmute_after_screensaver
bool ScreensaverControlService::getUnmuteAfterScreensaver() {
	return settings->value("mute_for_screensaver",
	SCREENSAVER_DEFAULT_UNMUTE_AFTER_SCREENSAVER).toBool();
}
void ScreensaverControlService::setUnmuteAfterScreensaver(
		bool unmute_after_screensaver) {
	if (getUnmuteAfterScreensaver() != unmute_after_screensaver) {
		settings->setValue("unmute_after_screensaver",
				unmute_after_screensaver);
		emit propertiesChanged("unmute_after_screensaver");
	}
}
void ScreensaverControlService::resetUnmuteAfterScreensaver() {
	setUnmuteAfterScreensaver(SCREENSAVER_DEFAULT_UNMUTE_AFTER_SCREENSAVER);
}

// playback_inhibits_screensaver
bool ScreensaverControlService::getPlaybackInhibitsScreensaver() {
	return settings->value("playback_inhibits_screensaver",
	SCREENSAVER_DEFAULT_PLAYBACK_INHIBITS_SCREENSAVER).toBool();
}
void ScreensaverControlService::setPlaybackInhibitsScreensaver(
		bool playback_inhibits_screensaver) {
	if (getPlaybackInhibitsScreensaver() != playback_inhibits_screensaver) {
		settings->setValue("playback_inhibits_screensaver",
				playback_inhibits_screensaver);
		emit propertiesChanged("playback_inhibits_screensaver");
	}
}
void ScreensaverControlService::resetPlaybackInhibitsScreensaver() {
	setPlaybackInhibitsScreensaver(
	SCREENSAVER_DEFAULT_PLAYBACK_INHIBITS_SCREENSAVER);
}

// audio_inhibits_screensaver
bool ScreensaverControlService::getAudioInhibitsScreensaver() {
	return settings->value("audio_inhibits_screensaver",
	SCREENSAVER_DEFAULT_AUDIO_INHIBITS_SCREENSAVER).toBool();
}
void ScreensaverControlService::setAudioInhibitsScreensaver(
		bool audio_inhibits_screensaver) {
	if (getAudioInhibitsScreensaver() != audio_inhibits_screensaver) {
		settings->setValue("audio_inhibits_screensaver",
				audio_inhibits_screensaver);
		emit propertiesChanged("audio_inhibits_screensaver");
		if (audio_inhibits_screensaver) {
			DBOUT("emit(pausePulseService());");
			pulseService->unpauseAsync();
		} else {
			DBOUT("emit(unpausePulseService());");
			pulseService->pauseAsync();
		}
	}
}
void ScreensaverControlService::resetAudioInhibitsScreensaver() {
	setAudioInhibitsScreensaver(SCREENSAVER_DEFAULT_AUDIO_INHIBITS_SCREENSAVER);
}

// audio_sleep_inhibit_power
double ScreensaverControlService::getAudioSleepInhibitPower() {
	return settings->value("audio_sleep_inhibit_power",
	SCREENSAVER_AUDIO_DEFAULT_SLEEP_INHIBIT_POWER).toDouble();
}
void ScreensaverControlService::setAudioSleepInhibitPower(
		double audio_inhibit_power) {
	if (audio_inhibit_power < SCREENSAVER_AUDIO_MIN_SLEEP_INHIBIT_POWER) {
		audio_inhibit_power = SCREENSAVER_AUDIO_MIN_SLEEP_INHIBIT_POWER;
	}
	if (audio_inhibit_power > SCREENSAVER_AUDIO_MAX_SLEEP_INHIBIT_POWER) {
		audio_inhibit_power = SCREENSAVER_AUDIO_MAX_SLEEP_INHIBIT_POWER;
	}
	if (getAudioSleepInhibitPower() != audio_inhibit_power) {
		settings->setValue("audio_sleep_inhibit_power", audio_inhibit_power);
		emit propertiesChanged("audio_sleep_inhibit_power");
	}
}
void ScreensaverControlService::resetAudioSleepInhibitPower() {
	setAudioSleepInhibitPower(SCREENSAVER_AUDIO_DEFAULT_SLEEP_INHIBIT_POWER);
}

// audio_observation_period
double ScreensaverControlService::getAudioObservationPeriod() {
	return settings->value("audio_observation_period",
	SCREENSAVER_AUDIO_DEFAULT_OBSERVATION_PERIOD).toDouble();
}
void ScreensaverControlService::setAudioObservationPeriod(
		double observationPeriod) {
	if (getAudioObservationPeriod() != observationPeriod) {
		settings->setValue("audio_observation_period", observationPeriod);
		pulseService->setObservationPeriod(observationPeriod);
		emit propertiesChanged("audio_observation_period");
	}
}
void ScreensaverControlService::resetAudioObservationPeriod() {
	setAudioObservationPeriod(SCREENSAVER_AUDIO_DEFAULT_OBSERVATION_PERIOD);
}

// notifications_enabled
bool ScreensaverControlService::getNotificationsEnabled() {
	return settings->value("notifications_enabled",
	SCREENSAVER_DEFAULT_NOTIFICATIONS_ENABLED).toBool();
}
void ScreensaverControlService::setNotificationsEnabled(
		bool notifications_enabled) {
	if (getNotificationsEnabled() != notifications_enabled) {
		settings->setValue("notifications_enabled", notifications_enabled);
		emit propertiesChanged("notifications_enabled");
	}
}
void ScreensaverControlService::resetNotificationsEnabled() {
	setNotificationsEnabled(SCREENSAVER_DEFAULT_NOTIFICATIONS_ENABLED);
}

// notification_duration
int ScreensaverControlService::getNotificationDuration() {
	return settings->value("notification_duration",
	SCREENSAVER_DEFAULT_NOTIFICATION_DURATION).toInt();
}
void ScreensaverControlService::setNotificationDuration(
		int notification_duration) {
	if (getNotificationDuration() != notification_duration) {
		settings->setValue("notification_duration", notification_duration);
		emit propertiesChanged("notification_duration");
	}
}
void ScreensaverControlService::resetNotificationDuration() {
	setNotificationDuration(SCREENSAVER_DEFAULT_NOTIFICATION_DURATION);
}

