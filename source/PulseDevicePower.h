#pragma once

#include <pulse/pulseaudio.h>
#include <QtCore/QString>
#include <QtCore/QMap>

double pa_get_source_power(QString device, double duration);
int pa_get_monitor_power_map(QMap<QString, double> *monitorPowers, double duration);
