#include "PulseService.h"
#include "PulseDevicePower.h"
#include "PulseMute.h"
#include "dbout.h"

#include <cmath>
#include <iostream>
#include <QtCore/QDebug>
#include <QtCore/QMap>
#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtCore/QWaitCondition>
#include <unistd.h>

PulseService::PulseService(double observationPeriod, QObject *parent) :
		QObject(parent) {
	assert(observationPeriod > 0);
	this->observationPeriod = observationPeriod;
	currState = stopped;
	nextState = stopped;
	QMutex mutex;
	QWaitCondition stateChangedCondition;
}

bool PulseService::isRunning() {
	return currState == running;
}

bool PulseService::isPaused() {
	return currState == paused;
}

bool PulseService::isStopped() {
	return currState == stopped;
}

void PulseService::run() {
	mutex.lock();
	currState = running;
	nextState = running;
	DBOUT("run() method called.");
	while (true) {
		if (currState != nextState) {
			currState = nextState;
			currStateChangedCondition.wakeAll();
			if (currState == stopped) {
				break;
			}
			if (currState == paused) {
				nextStateChangedCondition.wait(&mutex);
			}
		}
		mutex.unlock();
		double observationPeriod_numParts = ceil(observationPeriod);
        double observationPeriod_partLength = observationPeriod / observationPeriod_numParts;
		double totalPower = 0;
		for (int i = 0; i < observationPeriod_numParts; i++) {
			DBOUT("MEASURING... period=" << observationPeriod << "part=" << observationPeriod_partLength);
			if (nextState != running) {
				break;
			}
			// Break the measurement into one-second chunks, so that it can be stopped.
            QMap<QString, double> monitorPowers;
            if (pa_get_monitor_power_map(&monitorPowers, observationPeriod_partLength) != 0) {
				std::cerr << "pa_get_monitor_power_map() failure." << std::endl;
			} else {
				double maxVolume = 0;
				QMap<QString, double>::iterator it;
                for (it = monitorPowers.begin(); it != monitorPowers.end(); ++it) {
					if (it.value() > maxVolume) {
						maxVolume = it.value();
					}
				}
				totalPower += maxVolume * observationPeriod_partLength;
			}
			DBOUT("POWER MEASURED");
		}
		mutex.lock();
		if (nextState == running) {
			double averagePower = totalPower / observationPeriod;
			DBOUT("average power measured:" << averagePower);
			emit powerMeasured(averagePower);
		}
	}
	mutex.unlock();
	DBOUT("PulseService stopped.");
}

bool PulseService::pause() {
	DBOUT("PulseService pause()");
	mutex.lock();
	if (currState == running) {
		if (nextState != paused) {
			nextState = paused;
			nextStateChangedCondition.wakeAll();
		}
		currStateChangedCondition.wait(&mutex);
	}
	mutex.unlock();
	return currState == paused;
}

void PulseService::pauseAsync() {
	DBOUT("PulseService::pauseAsync()");
	mutex.lock();
	if (currState == running) {
		if (nextState != paused) {
			nextState = paused;
			nextStateChangedCondition.wakeAll();
		}
	}
	mutex.unlock();
}

bool PulseService::unpause() {
	DBOUT("PulseService unpause()");
	mutex.lock();
	if (currState == paused) {
		if (nextState != running) {
			nextState = running;
			nextStateChangedCondition.wakeAll();
		}
		currStateChangedCondition.wait(&mutex);
	}
	mutex.unlock();
	return currState == running;
}

void PulseService::unpauseAsync() {
	DBOUT("PulseService::unpauseAsync()");
	mutex.lock();
	if (currState == paused) {
		if (nextState != running) {
			nextState = running;
			nextStateChangedCondition.wakeAll();
		}
	}
	mutex.unlock();
}

bool PulseService::stop() {
	DBOUT("PulseService stop()");
	mutex.lock();
	if (currState == running || nextState == paused) {
		if (nextState != stopped) {
			nextState = stopped;
			nextStateChangedCondition.wakeAll();
		}
		currStateChangedCondition.wait(&mutex);
	}
	mutex.unlock();
	return currState == stopped;
}

double PulseService::getObservationPeriod() {
	return observationPeriod;
}

void PulseService::setObservationPeriod(double observationPeriod) {
	assert(observationPeriod > 0);
	this->observationPeriod = observationPeriod;
	DBOUT("PulseService observation period set to " << observationPeriod);
}

bool PulseService::isMute() {
	return pa_get_muted();
}

void PulseService::setMute(bool muted) {
	pa_set_muted(muted);
}
