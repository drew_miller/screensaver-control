#include "PulseMute.h"

#include <pulse/context.h>
#include <pulse/error.h>
#include <pulse/pulseaudio.h>
#include <QtCore/QMap>
#include <QtCore/QMutex>
#include <QtCore/QWaitCondition>
#include <iostream>

struct getMute_struct {
	getMute_struct(pa_mainloop_api* mainloop_api,
			QMap<quint32, bool>* sinkStates) :
			mainloop_api(mainloop_api), sinkStates(sinkStates) {
		sinksWaiting = 0;
	}
	pa_mainloop_api* mainloop_api;
	QMap<quint32, bool>* sinkStates;
	int sinksWaiting;
	QMutex mutex;
	QWaitCondition waitCondition;
};

void pa_getMute_sinklist_cb(pa_context *c, const pa_sink_info *i, int eol,
		void *userdata) {
	getMute_struct* data = (getMute_struct*) userdata;
	if (!eol) {
		data->mutex.lock();
		data->sinkStates->insert(i->index, !(bool) (i->mute));
		data->waitCondition.wakeAll();
		data->mutex.unlock();
	} else {
		data->mutex.lock();
		data->mutex.unlock();
		pa_context_disconnect(c);
	}
}

void pa_getMute_context_state_cb(pa_context *c, void *userdata) {
	getMute_struct* data = (getMute_struct*) userdata;
	pa_context_state_t state = pa_context_get_state(c);
	if (state == PA_CONTEXT_READY) {
		pa_operation* o;
		if (!(o = pa_context_get_sink_info_list(c, pa_getMute_sinklist_cb,
				userdata))) {
			std::cerr << "pa_context_get_sink_info_list() failed." << std::endl;
			std::cerr << pa_strerror(pa_context_errno(c)) << std::endl;
		} else {
			pa_operation_unref(o);
		}
	} else if (state == PA_CONTEXT_TERMINATED) {
		// Context cleanly closed
		pa_mainloop_api* mainloop_api = data->mainloop_api;
		mainloop_api->quit(mainloop_api, 0);
	} else if (state == PA_CONTEXT_FAILED) {
		// An error has occurred in the context
		pa_mainloop_api* mainloop_api = data->mainloop_api;
		mainloop_api->quit(mainloop_api, 1);
	}
}

bool pa_get_muted(QMap<quint32, bool>* sinkStates) {
	bool deleteSinkStates = true;
	if (sinkStates == NULL) {
		sinkStates = new QMap<quint32, bool>();
		deleteSinkStates = true;
	}
	// Create a mainloop API and connection to the default server
	pa_mainloop* mainloop = pa_mainloop_new();
	pa_mainloop_api* mainloop_api = pa_mainloop_get_api(mainloop);
	pa_context* pa_ctx = pa_context_new(mainloop_api, "PulseGetMute");
	// The mute_struct parameter is passed around all of these pa_mute methods.
	getMute_struct* data = new getMute_struct(mainloop_api, sinkStates);
	pa_context_set_state_callback(pa_ctx, pa_getMute_context_state_cb, data);
	if (pa_context_connect(pa_ctx, NULL, PA_CONTEXT_NOFLAGS, NULL) < 0) {
		if (deleteSinkStates) {
			delete sinkStates;
		}
		delete data;
		return false;
	}
	// Run the mainloop.
	int err = 0;
	pa_mainloop_run(mainloop, &err);
	if (pa_ctx) {
		pa_context_unref(pa_ctx);
	}
	if (mainloop) {
		pa_signal_done();
		pa_mainloop_free(mainloop);
	}
	delete data;
	bool returnValue = true;
	for (bool state : sinkStates->values()) {
		if (state) {
			returnValue = false;
			break;
		}
	}
	if (deleteSinkStates) {
		delete sinkStates;
	}
	return returnValue;
}

void pa_set_muted(bool muted) {
	// The stupid PulseAudio API setters don't seem to work, and it's too poorly
	// documented to try to debug. Thus the amixer dependency.
	if (muted) {
		if (system("amixer -D pulse -q sset Master mute")) {
			std::cerr << "failed to mute volume." << std::endl;
		}
	} else {
		if (system("amixer -D pulse -q sset Master unmute")) {
			std::cerr << "failed to unmute volume." << std::endl;
		}
	}
}
