#pragma once

#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QWaitCondition>

#define DEFAULT_AUDIO_OBSERVATION_PERIOD 5

class PulseService: public QObject {
Q_OBJECT

public:
	explicit PulseService(double observationPeriod =
	DEFAULT_AUDIO_OBSERVATION_PERIOD, QObject *parent = NULL);
	bool isRunning();
	bool isPaused();
	bool isStopped();

private:
	enum RunState {
		running, stopped, paused
	};
	RunState currState;
	RunState nextState;
	double observationPeriod;
	QMutex mutex;
	QWaitCondition currStateChangedCondition;
	QWaitCondition nextStateChangedCondition;

public slots:
	// Run the service
	void run();
	bool pause();
	void pauseAsync();
	bool unpause();
	void unpauseAsync();
	bool stop();
	double getObservationPeriod();
	void setObservationPeriod(double observationPeriod);
	bool isMute();
	void setMute(bool mute);

signals:
	void powerMeasured(double power);

};
