#pragma once

#include <pulse/pulseaudio.h>
#include <QtCore/QMap>
#include <QtCore/QSet>

bool pa_get_muted(QMap<quint32, bool>* sinkStates = NULL);
void pa_set_muted(bool muted);
