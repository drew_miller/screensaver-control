#include "ScreensaverControlService.h"
#include "Config.h"

#include <iostream>

#include <QtCore/qnamespace.h>
#include <QtCore/QCoreApplication>
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QRegExp>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QTimer>

void printHelp() {
	std::cout << "Screensaver Settings" << std::endl;
	std::cout << "Manages media playback and the screensaver." << std::endl;
	std::cout
			<< "Syntax: screensaver-settings [--help] [--reset] [--no_run] [--setting=value]"
			<< std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "--help: Prints this help dialog." << std::endl;
	std::cout << "--reset: Clears all settings." << std::endl;
	std::cout << "--no_run: Apply settings and quit." << std::endl;
	std::cout << "--setting=value: Set setting to value. Recognized settings:"
			<< std::endl;
	std::cout << "  * properties_version." << std::endl;
	std::cout << "      default=" << SCREENSAVER_PROPERTIES_VERSION_CURR
			<< std::endl;
	std::cout
			<< "  * pause_for_screensaver: Pause music/video playback when the screensaver starts."
			<< std::endl;
	std::cout << "      default=" << SCREENSAVER_DEFAULT_PAUSE_FOR_SCREENSAVER
			<< std::endl;
	std::cout
			<< "  * playback_inhibits_screensaver: Don't automatically go to screensaver when a media player is playing."
			<< std::endl;
	std::cout << "      default="
			<< SCREENSAVER_DEFAULT_PLAYBACK_INHIBITS_SCREENSAVER << std::endl;
	std::cout
			<< "  * audio_inhibits_screensaver: Whether audio above audio_sleep_inhibit_power should prevent automatically going to screensaver."
			<< std::endl;
	std::cout << "      default=" << SCREENSAVER_DEFAULT_AUDIO_INHIBITS_SCREENSAVER
			<< std::endl;
	std::cout
			<< "  * resume_after_screensaver: Whether playback should be immediately resumed when returning from the screensaver."
			<< std::endl;
	std::cout << "      default=" << SCREENSAVER_DEFAULT_RESUME_AFTER_SCREENSAVER
			<< std::endl;
	std::cout
			<< "  * show_notification: Show a notification asking if playback should be resumed/cancelled when returning from screensaver."
			<< std::endl;
	std::cout << "      default="
			<< SCREENSAVER_DEFAULT_NOTIFICATIONS_ENABLED << std::endl;
	std::cout
			<< "  * audio_observation_period: Time window (in seconds) used when measuring audio_sleep_inhibit_power."
			<< std::endl;
	std::cout << "      default=" << SCREENSAVER_AUDIO_DEFAULT_OBSERVATION_PERIOD
			<< std::endl;
	std::cout
			<< "  * audio_sleep_inhibit_power: The higher this value is, the louder sound must be to prevent automatic screensaver."
			<< std::endl;
	std::cout << "      default=" << SCREENSAVER_AUDIO_DEFAULT_SLEEP_INHIBIT_POWER
			<< std::endl;
	std::cout
			<< "  * notification_duration: Time in milliseconds this application's notifications should last."
			<< std::endl;
	std::cout << "      default=" << SCREENSAVER_DEFAULT_NOTIFICATION_DURATION
			<< std::endl;
	std::cout << std::endl;
}

int main(int argc, char *argv[]) {
	// Run this as a QCoreApplication.
	QCoreApplication* a = new QCoreApplication(argc, argv);

	// Use QSettings to save stuff.
	QSettings* settings = new QSettings("CravenCompany", "ScreensaverControlService");

	// Parse the arguments.
	bool noRun = false;
	QList<QString> arguments = QCoreApplication::arguments();
	if (arguments.contains("--help")) {
		printHelp();
		noRun = true;
	} else {
		for (QString argument : arguments) {
			if (argument == "--no_run") {
				noRun = true;
			} else if (argument == "--reset") {
				settings->clear();
			} else if (argument.contains(QRegExp("^\\-\\-[\\d\\w]+="))) {
				QList<QString> pair = argument.split("=",
						QString::SplitBehavior::KeepEmptyParts);
				if (pair.size() == 2) {
					settings->setValue(pair.value(0).mid(2), pair.value(1));
				}
			}
		}
	}

	// Apply settings, but don't run the program.
	if (noRun) {
		delete settings;
		delete a;
		return 0;
	}

	// ScreensaverControlService runs the other services.
	ScreensaverControlService* ps = new ScreensaverControlService(settings, a);

	// Run the loop from the application event loop.
	QTimer::singleShot(0, ps, SLOT(run()));

	// Run the QCoreApplication.
	int retVal = a->exec();

	delete a;
	delete settings;
	delete ps;

	// Return the QCoreApplication's return state.
	return retVal;
}

