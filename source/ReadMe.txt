= Screensaver Control =

== About ==
"Prevent your computer from automatically going to sleep when sound is playing. 
Pause media playback when locking the screen, and optionally resume playback 
when logging back in."

Screensaver Control is a service that listens to the internal audio playing on your 
computer, and tells the computer not to go to sleep if the sound power passes 
beyond a specified threshold. Screensaver Control can be interacted over its 
DBus session interface at com.ScreensaverControl, where it broadcasts the power 
of the PulseAudio sound playing. This service is intended to be configured by 
the Screensaver Config app.

== System Requirements ==
You need to have the following libraries installed:
 * libpulse0. For development, you also need libpulse-dev.
 * libqt5dbus5. libqt4dbus4 may work, but the CMake build setup would have to 
       be tweaked.
 * notify-osd. 

== Building the program ==
sudo apt-get install ninja-build # ninja build system (like make, but better).
sudo apt-get install g++ # Or any other CXX compiler with C++0x support.
sudo apt-get install qt5-default # You probably don't need the whole metapackage, but this'll do it.
sudo apt-get install libpulse-dev # PulseAudio library
chmod +x build.sh # Make build.sh executable
./build.sh --debug # debug build
./build.sh --release # release build
./build.sh --package # packaging build

== Running the program ==
release/screensaver-control

== Installation ==
./build.sh --package # packaging build (see section Building the Program).
cd package
sudo cpack
sudo apt-get install

== Checking that it's working ==
Inspect the output of the program with qdbusviewer or d-feet. qdbusviewer has 
an issue where it can't be properly added to the Unity system menu, but you can 
start it up with Alt+F2. The advantage qdbusviewer has over d-feet is that 
qdbusviewer can listen to signal outputs.

#qdbusviewer
sudo apt-get install qdbusviewer
qdbusviewer
#d-feet
sudo apt-get install d-feet
d-feet

Then search for com.ScreensaverControl on the D-Bus with qdbusviewer, and run 
its methods. You should be able to set the observationPeriod, which is how 
frequently (seconds per DBus broadcast) that the program broadcasts the power 
of the Pulseaudio system to the D-Bus. The recommended value for this is 5.

If you have screensaver-config installed, change settings with that application 
and check to make sure they are applied.

Resources:
 * DBus clients:
 	http://techbase.kde.org/Development/Tutorials/D-Bus/Accessing_Interfaces
 * DBus interfaces:
 	http://techbase.kde.org/Development/Tutorials/D-Bus/Creating_Interfaces
 * DBus watcher:
 	Better than continually listing the services on the DBus to see if a new audio player has connected.
 	http://qt-project.org/doc/qt-4.8/qdbusservicewatcher.html
 * QThread:
 	used because the DBus locks up if it's running in the same thread as the PulseService
 	http://stackoverflow.com/questions/11033971/qt-thread-with-movetothread
 	http://qt-project.org/doc/qt-4.8/qthread.html
 * Signals and Slots:
 	The basic mechanism used by Qt to communicate between threads.
 	http://qt-project.org/doc/qt-4.8/signalsandslots.html
 * Ninja build system:
 	http://neugierig.org/software/chromium/notes/2011/02/ninja.html
 * Taking a screenshot with XLib:
	http://stackoverflow.com/questions/8249669/how-do-take-a-screenshot-correctly-with-xlib
 * Recording audio from PulseAudio:
	http://stackoverflow.com/questions/14454852/pulseaudio-recording-and-playback-fails


== License ==
This software is licensed under the GPLv3.
https://www.gnu.org/licenses/gpl-3.0.txt


// Drew Miller.
dmiller309@gmail.com
