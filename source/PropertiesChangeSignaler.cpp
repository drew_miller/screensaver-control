#include "PropertiesChangeSignaler.h"

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QVariant>

PropertiesChangeSignaler::PropertiesChangeSignaler(QString source,
		QObject *parent) :
		QObject(parent) {
	this->source = source;
}

QString PropertiesChangeSignaler::getSource() {
	return source;
}

void PropertiesChangeSignaler::propertiesChanged_slot(QString interface_name,
		QMap<QString, QVariant> changed_properties) {
	// TODO: add third argument, ARRAY<STRING> invalidated_properties
	emit propertiesChanged(source, interface_name, changed_properties);
}
