#pragma once

#include "x_assert.h"
#include "PropertiesChangeSignaler.h"
#include "PulseService.h"

#include <QtCore/QDateTime>
#include <QtDBus/QDBusConnection>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QSet>
#include <QtCore/QSettings>
#include <QtCore/QString>
#include <QtCore/QVariant>
#include <QtCore/QVariantMap>

class ScreensaverControlService: public QObject {
Q_OBJECT
Q_CLASSINFO("D-Bus Interface", "com.ScreensaverControl")

public:
	// Constructor/Destructor
	explicit ScreensaverControlService(QSettings* settings, QObject *parent = 0);
	virtual ~ScreensaverControlService();

	// Methods
	void dBusServiceRegistered(QString name);
	void dBusServiceUnregistered(QString name);
	void resetToDefault();
	void showNotification();
	void closeNotification();

	// Properties
	Q_PROPERTY(int properties_version
			READ getPropertiesVersion
			WRITE setPropertiesVersion
			RESET resetPropertiesVersion
            SCRIPTABLE true)

    Q_PROPERTY(bool pause_for_screensaver
			READ getPauseForScreensaver
			WRITE setPauseForScreensaver
			RESET resetPauseForScreensaver
			SCRIPTABLE true)

    Q_PROPERTY(bool resume_after_screensaver
			READ getResumeAfterScreensaver
			WRITE setResumeAfterScreensaver
			RESET resetResumeAfterScreensaver
			SCRIPTABLE true)

    Q_PROPERTY(bool mute_for_screensaver
			READ getMuteForScreensaver
			WRITE setMuteForScreensaver
			RESET resetMuteForScreensaver
			SCRIPTABLE true)

    Q_PROPERTY(bool unmute_after_screensaver
			READ getUnmuteAfterScreensaver
			WRITE setUnmuteAfterScreensaver
			RESET resetUnmuteAfterScreensaver
			SCRIPTABLE true)

    Q_PROPERTY(bool playback_inhibits_screensaver
			READ getPlaybackInhibitsScreensaver
			WRITE setPlaybackInhibitsScreensaver
			RESET resetPlaybackInhibitsScreensaver
			SCRIPTABLE true)

    Q_PROPERTY(bool audio_inhibits_screensaver
			READ getAudioInhibitsScreensaver
			WRITE setAudioInhibitsScreensaver
			RESET resetAudioInhibitsScreensaver
			SCRIPTABLE true)

    Q_PROPERTY(double audio_sleep_inhibit_power
			READ getAudioSleepInhibitPower
			WRITE setAudioSleepInhibitPower
			RESET resetAudioSleepInhibitPower
			SCRIPTABLE true)

    Q_PROPERTY(double audio_observation_period
			READ getAudioObservationPeriod
			WRITE setAudioObservationPeriod
			RESET resetAudioObservationPeriod
			SCRIPTABLE true)

    Q_PROPERTY(bool notifications_enabled
			READ getNotificationsEnabled
			WRITE setNotificationsEnabled
			RESET resetNotificationsEnabled
			SCRIPTABLE true)

    Q_PROPERTY(int notification_duration
			READ getNotificationDuration
			WRITE setNotificationDuration
			RESET resetNotificationDuration
			SCRIPTABLE true)

public slots:
	void run();
    void handle_powerMeasured(double power);
    void handle_screenLocked();
    void handle_screenUnlocked();
    void handle_screenSaverActiveChanged(bool active);
    void handle_playerPropertiesChanged(QString source, QString interface_name,
			QMap<QString, QVariant> changed_properties);
    void handle_dBusNameOwnerChanged(QString name, QString old_owner,
			QString new_owner);
    void handle_notificationActionInvoked(uint id, QString action_key);
	/**
	 *  The notification has been closed.
	 *	Reasons that the notification can be closed:
	 *	1 - The notification expired.
	 *	2 - The notification was dismissed by the user.
	 *	3 - The notification was closed by a call to CloseNotification.
	 *	4 - Undefined/reserved reasons.
	 */
    void handle_notificationClosed(uint id, uint reason);

	// Property slots
	// properties_version
	int getPropertiesVersion();
	void setPropertiesVersion(int propertiesVersion);
	void resetPropertiesVersion();
	// pause_for_screensaver
	bool getPauseForScreensaver();
	void setPauseForScreensaver(const bool &pauseForScreensaver);
	void resetPauseForScreensaver();
	// resume_after_screensaver
	bool getResumeAfterScreensaver();
	void setResumeAfterScreensaver(bool resume_after_screensaver);
	void resetResumeAfterScreensaver();
	// mute_for_screensaver
	bool getMuteForScreensaver();
	void setMuteForScreensaver(bool mute_for_screensaver);
	void resetMuteForScreensaver();
	// unmute_after_screensaver
	bool getUnmuteAfterScreensaver();
	void setUnmuteAfterScreensaver(bool mute_for_screensaver);
	void resetUnmuteAfterScreensaver();
	// playback_inhibits_screensaver
	bool getPlaybackInhibitsScreensaver();
	void setPlaybackInhibitsScreensaver(bool playback_inhibits_screensaver);
	void resetPlaybackInhibitsScreensaver();
	// audio_inhibits_screensaver
	bool getAudioInhibitsScreensaver();
	void setAudioInhibitsScreensaver(bool audio_inhibits_screensaver);
	void resetAudioInhibitsScreensaver();
	// notifications_enabled
	bool getNotificationsEnabled();
	void setNotificationsEnabled(bool show_playback_resume);
	void resetNotificationsEnabled();
	// audio_sleep_inhibit_power
	double getAudioSleepInhibitPower();
	void setAudioSleepInhibitPower(double sleep_inhibit_power);
	void resetAudioSleepInhibitPower();
	// audio_observation_period
	double getAudioObservationPeriod();
	void setAudioObservationPeriod(double audio_observation_period);
	void resetAudioObservationPeriod();
	// notification_duration
	int getNotificationDuration();
	void setNotificationDuration(int notification_duration);
	void resetNotificationDuration();
	//
	Q_SCRIPTABLE
	int countPlayingMediaPlayers();
	//
	Q_SCRIPTABLE
	void resetToDefaults();

signals:
	//
	Q_SCRIPTABLE
	void numPlayingPlayersCountChanged(int numPlayingPlayers);
	//
	Q_SCRIPTABLE
	void powerMeasured(double power);
	//
	Q_SCRIPTABLE
	void propertiesChanged(QString property);
	//
	// TODO: Correctly support the PropertiesChanged DBus Properties method:
	//       http://www.mentby.com/Group/dbus/propertieschanged-signal-take-2.html
	//       It should be like this:
	//	void propertiesChangedT(QString name, QMap<QString, QVariant> values,
	//			QList<QString> invalidated_values); //

private:
	// Methods:
	QString getPlaybackStatus(QString player);
	void setPlaybackStatus(QString player, QString status);
	bool getScreensaverState();
	void setScreensaverInhibited(bool inhibited);

	// Properties:
	QSettings* settings;
	QDBusConnection dbus = QDBusConnection::sessionBus();
	QThread* pulseService_thread;
	PulseService* pulseService;
	QMap<QString, PropertiesChangeSignaler*> signalers;
	QMap<QString, QString> playbackStatuses;
	QSet<QString> pausedPlayers;
	QSet<QString> unpausingPlayers;
	QDateTime* lastUnpause;
	bool isNotificationShowing;
	bool isScreensaverActive;
	int numPlayingPlayers;
	uint screensaverInhibitCookie;
	bool isScreensaverInhibited;
	bool mutedByScreensaver;
	quint32 notificationId;

};
