#!/bin/bash
set -e

if [[ "$1" -eq "--all" || "$1" == "--debug" ]]
then
	mkdir -p builds/debug
	pushd debug
#	rm -rf *
	cmake -GNinja -DCMAKE_BUILD_TYPE=DEBUG ../source
	ninja
	popd
fi
if [[ "$1" == "--all" || "$1" == "--release" ]]
then
	mkdir -p builds/release
	pushd release
#	rm -rf *
	cmake -GNinja -DCMAKE_BUILD_TYPE=RELEASE ../source
	ninja
	popd
fi
if [[ "$1" == "--all" || "$1" == "--package" ]]
then
	mkdir -p builds/package
	pushd package
#	rm -rf *
	cmake -DCMAKE_BUILD_TYPE=PACKAGE ../source
	popd
fi
